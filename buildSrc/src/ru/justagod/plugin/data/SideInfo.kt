package ru.justagod.plugin.data

data class SideInfo internal constructor(val name: String)