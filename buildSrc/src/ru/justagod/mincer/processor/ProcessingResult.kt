package ru.justagod.mincer.processor

enum class ProcessingResult {
    REWRITE, NOOP, DELETE
}