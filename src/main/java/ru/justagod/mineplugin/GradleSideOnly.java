package ru.justagod.mineplugin;

/**
 * Created by JustAGod on 08.03.2018.
 */
@SuppressWarnings("unused")
public @interface GradleSideOnly {
    GradleSide[] value();
}
